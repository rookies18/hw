


#include <iostream>
#include <string>
#include <istream>
#include <assert.h>
#include <limits.h>

#include "sceduler.h"
#include "robot.h"
#include "movecontrol.h"






    

void Sceduler::Init(){
    // 获取第一次输入的地图信息
    char line[1024];

    while (fgets(line, sizeof line, stdin)) {
        if (line[0] == 'O' && line[1] == 'K') {
            break;
        }
        // 消息保存在Msg中,按行保存
        _msg.push_back(std::string(line));
    }
    // 根据地图初始化工作台和小车
    for(int n = 0;n<_msg.size();n++){
        auto l = _msg[n];
        for(int i = 0;i<l.size();++i){
            if(l[i] != '.'){
                int m = i;
                // 遇到了换行符,本行结束,fgets会读取换行符
                if(l[i] == '\n' || l[i] == '\r'){
                    // 进入下一行
                    break;
                // 遇到机器人
                } else if( l[i] == 'A' ) {
                    // 获取id
                    int id = _robot_list.size();
                    // 获取坐标
                    double lc[2] = {m*0.5+0.25,n*0.5+0.25};
                    _robot_list.push_back(Robot::ptr(new Robot(id,lc[0],lc[1])));        
                    
                // 遇到工作台
                } else if( l[i]<='9' && l[i]>='0' ){
                    // 根据行列计算坐标
                    double lc[2] = {m*0.5+0.25,n*0.5+0.25};
                    // 根据当前长度计算id(id就是vector中的下标)
                    int id = _staging_list.size();
                    // 获取type
                    int type = l[i]-'0';
                    // 根据id,坐标和type加入新的工作台

                    /// 完成工作台映射表的初始化.
                    for(int j = 0;j<_staging_list.size();++j){
                        if(j == id){
                            _dis_map[j][id] = 0;
                            continue;
                        }
                        auto p = _staging_list[j]->getLocation();
                        int times = timeMin(std::vector<double>({lc[0],lc[1]}),p);
                        _dis_map[j][id] = times;
                    }
                    auto ts = shared_from_this();
                    _staging_list.push_back(Staging::ptr(new Staging(id,lc,type,ts)));
                }
            }
        }
    }
    _k = _staging_list.size();
    _msg.clear();

    /// 根据工作台映射表,初始化每一个工作台的nearlist和nextlist;
    for(int i = 0;i<_k;i++){
        auto it = _staging_list[i];
        int type = it->getType();
        // 0,1,2,4,8,16,32,64,128,256
        int nextType = 0;

        switch (type)
        {
        case 1:
            nextType = 8 | 16 | 256;
            break;
        case 2:
            nextType = 8 | 32 | 256;
            break;
        case 3:
            nextType = 16 | 32 | 256;
            break;  
        case 4:
            nextType = 64 | 256;
            break; 
        case 5:
            nextType = 64 | 256;
            break;   
        case 6:
            nextType = 64 | 256;
            break;
        case 7:
            nextType = 128 | 256;
        default:
            nextType = 0;
            break;
        }
        std::priority_queue< std::pair<int,int>, std::vector< std::pair<int,int> >, std::less< std::pair< int,int > > > nearlist;
        std::priority_queue< std::pair<int,int>, std::vector< std::pair<int,int> >, std::greater< std::pair< int,int > > > nextlist;
        for(int j = 0;j<_k;j++){
            if(i == j){
                continue;
            }
            auto tmp = _staging_list[j];

            if(nextType & (1<<(tmp->getType()-1)) ){
                nextlist.push(std::make_pair(_dis_map[i][j],j));
            }

            if(nearlist.size() < 25){
                nearlist.push(std::make_pair(_dis_map[i][j],j));
            }else if(_dis_map[i][j] < nearlist.top().first){
                nearlist.pop();
                nearlist.push(std::make_pair(_dis_map[i][j],j));
            }
        }
        while(!nearlist.empty()){
            it->addNear(nearlist.top());
            nearlist.pop();
        }
        while(!nextlist.empty()){
            it->addNext(nextlist.top());
            nextlist.pop();
        }
        it->reverseNear();
        
    }

    /// 为每个机器人设置最初的目标
    for(int i = 0;i<_robot_list.size();++i){
        // 获取机器人指针
        auto it = _robot_list[i];
        // 获取位置
        auto lc = it->getLocation();
        // 位置必定是2位
        assert(lc.size() == 2);

        // 记录到最近工作台的距离
        int min = INT_MAX;
        // 记录最近的工作台id
        int index = -1;
        // 遍历工作台列表,获取最近的1,2,3工作台
        for(int j = 0;j<_staging_list.size();j++){
            auto st = _staging_list[j];
            auto p = st->getLocation();
            int times = timeMin(lc,p);
            if( min > times && st->getType() & (1|2|4)){
                min = times;
                index = j;
            }
        }
        it->setTarget(index);
    }

    
}



int Sceduler::getMsg(){
    char line[1024];

    while (fgets(line, sizeof line, stdin)) {
        if (line[0] == 'O' && line[1] == 'K') {
            break;
        }
        // 消息保存在Msg中,按行保存
        _msg.push_back(std::string(line));
    }
    int n = _msg.size();
    if(n == 0){
        return 0;
    }

    // msg不包括最后的OK,因此一定为开头两行加k个工作台和四个机器人
    if(n!=_k+6){
        return -1;
    }

    sscanf(_msg[0].c_str(),"%d %d",&_times,&_money);
    int k;
    sscanf(_msg[1].c_str(),"%d",&k);

    // k为工作台数量,必定等于_k;
    if(k != _k){
        return -1;
    }
    int i = 2;

    while(k > 0){
        Staging_Msg msg;
        sscanf(_msg[i].c_str(),"%d %lf %lf %d %d %d",
                &msg._id,&msg._x,&msg._y,&msg._remaining_time,&msg._keep_list,&msg._saled);
        
        /// 更新staging状态
        _staging_list[i-2]->refresh(msg);

        --k;
        ++i;
    }
    for(;i<n;++i){
        Robot_Msg msg;
        sscanf(_msg[i].c_str(),"%d %d %lf %lf %lf %lf %lf %lf %lf %lf",
            &msg._staging_id,&msg._keep,&msg._time_value,&msg._collide_value,
            &msg._w,&msg._v_x,&msg._v_y,&msg._angle,&msg._x,&msg._y);
        int id = i-_k-2;
        _robot_list[id]->refresh(msg);
    }
    return 1;
}

void Sceduler::sendMsg(){
    _send<<_times<<'\n';
    for(auto msg : _msgs->getCmd()){
        _send << msg.toString() <<'\n';
    }
    _send<<"OK"<<std::endl;
    std::cout<< _send.str();
    fflush(stdout);
}

int Sceduler::sendMsg_test(){
    MsgS::ptr msgs;
    Command cmd;
    cmd = (Command){"forward",0,true,4.5};
    msgs->addCMD(cmd);
    cmd = (Command){"rotate",0,true,3.14159};
    msgs->addCMD(cmd);
    cmd = (Command){"sell",1,false,0};
    msgs->addCMD(cmd);
    _send<<1140<<'\n';
    for(auto msg : _msgs->getCmd()){
        _send << msg.toString() <<'\n';
    }
    _send<<"OK"<<std::endl;
    std::cout<< _send.str();
    fflush(stdout);
}


void Sceduler::run(){
    Init();
    puts("OK");
    fflush(stdout);
    while(true){
       // 获取并解析消息
        int rt = getMsg();
        if(rt == 0){
            return;
        }
        assert(rt>0);
        RobotWork();
        sendMsg();
        _send.clear();
        _msgs->clear();
        _msg.clear();
    }
}

void Sceduler::RobotWork(){

    for(auto it : _robot_list){
        // 到了目的地,且是正确的目的地
        if(it->arriveTarget() && it->getSpotid() == it->getTargetId()){
            // 获取当前持有的类型
            int r_type = it->getThingtype();
            // 获取所处的工作台
            auto stag = Sceduler::_staging_list[it->getSpotid()];
            // 如果目前持有东西,要卖出去
            if(r_type){
                // 试着卖出去
                int rt = stag->buy(r_type);

                // 如果能卖
                if(rt>=0 && rt <50){
                    // 更新机器人状态
                    it->sell();
                    // 新建一行命令
                    Command msg;
                    msg._instruct = "sell";
                    msg._id = it->getId();
                    msg._hasThird = false;
                    // 命令设置为卖,并加入返回消息队列
                    _msgs->addCMD(msg);

                    // 重新设置目标
                    auto target = Sceduler::_staging_list[rt];
                    it->setTarget(rt);
                    //要获取下一步去哪的命令
                    // 如果换了新的目的地,就获取该目标生产产品的购买权
                    // 没换,说明没合适的去处
                    if(target->getId()!=stag->getId()){
                        target->book();
                    }

                    auto action = getMoveAction(it);
                    msg._instruct = "forward";
                    msg._hasThird = true;
                    msg._arg = action[0];
                    _msgs->addCMD(msg);
                    msg._instruct = "rotate";
                    msg._arg = action[1];
                    _msgs->addCMD(msg);
                    

                // rt错误，可能是类型不对，或者原材料栏满了
                } else {
                    //不应该
                }
            // 如果空手来的,就是来买东西的
            } else {
                int rt = stag->sale();
                if(rt>=0 && rt <50){
                    it->buy(stag->getSale());
                        Command msg;
                        msg._instruct = "buy";
                        msg._id = it->getId();
                        msg._hasThird = false;
                        // 命令设置为卖,并加入返回消息队列
                        _msgs->addCMD(msg);
                        // 重新设置目标
                        auto target = Sceduler::_staging_list[rt];
                        it->setTarget(rt);
                        //要获取下一步去哪的命令
                        auto action = getMoveAction(it);
                        msg._instruct = "forward";
                        msg._hasThird = true;
                        msg._arg = action[0];
                        _msgs->addCMD(msg);
                        msg._instruct = "rotate";
                        msg._arg = action[1];
                        _msgs->addCMD(msg);

                //没生产好
                } else {
                    // 如果正在生产,那就等着
                    if(stag->getRestTime() >= 0){
                        // 目的地不变
                        auto target = Sceduler::_staging_list[rt];
                        it->setTarget(rt);
                        //要获取下一步去哪的命令
                        auto action = getMoveAction(it);
                        msg._instruct = "forward";
                        msg._hasThird = true;
                        msg._arg = action[0];
                        _msgs->addCMD(msg);
                        msg._instruct = "rotate";
                        msg._arg = action[1];
                        _msgs->addCMD(msg);

                    //如果没在生产
                    } else {
                        // 就空手离开
                        int rt = stag->getSuggest(false);
                        auto target = Sceduler::_staging_list[rt];
                        it->setTarget(rt);
                        //要获取下一步去哪的命令
                        auto action = getMoveAction(it);
                        msg._instruct = "forward";
                        msg._hasThird = true;
                        msg._arg = action[0];
                        _msgs->addCMD(msg);
                        msg._instruct = "rotate";
                        msg._arg = action[1];
                        _msgs->addCMD(msg);
                        // 如果换了新的目的地,就获取该目标生产产品的购买权
                        // 没换,说明没合适的去处
                        if(target->getId()!=stag->getId()){
                            target->book();
                        }
                    }
                }
            }
        // 如果没到目的地
        } else {
            Command msg;
            auto action = getMoveAction(it);
            msg._instruct = "forward";
            msg._hasThird = true;
            msg._arg = action[0];
            _msgs->addCMD(msg);
            msg._instruct = "rotate";
            msg._arg = action[1];
            _msgs->addCMD(msg);
        }
    }
}



// vector<double> Sceduler:: howtoMove(int robot)
// {
//     auto r = _robot_list[i];
//     vector<double> control;
//     //已经到达目的地,重新找到目的地，设置目标，初始化新的控制器
//     if(r->arriveTarget()&& r->getSpotid() != -1)
//     {
//             //工作台返回的新目标x,y
//             vector<double> location = ;
//             //给机器人设置新目标
//             r->setTarget(location);
//             MoveController m = MoveController(r->getState(),location, r->getThingtype());
//             //析构之前的controller对象
//             r->_controller = make_shared<MoveController>(m);

//     }
//     //否则使用旧的controller
//     shared_ptr<MoveController> c = r->_controller;
        
        
//     //根据当前其他机器人的坐标设置障碍物
//     for(int k = 0;k<_robot_list.size(); k++){
//             if(k == i) continue;
//             auto other = _robot_list[k];
//             c->addObstacle(other->getState());
//     }
        
//     if(c->judgeCollide(r->getState()))
//     {
//             control = c->getCollideControl(r->getState());
//     }
//     else{
//             control = c->getControl(r->getState());
//     }
//     return control;
        
// }

// string Sceduler::whattoDo(int i)
// {
//      auto r = _robot_list[i];
//      if(r->arriveTarget()&& r->getSpotid() != -1)
//     {
        
//     }
// }

int Sceduler::getMinStag(int index){
    auto robot = _robot_list[index];
    int min = INT_MAX;
    int n = -1;
    for(auto it: _staging_list){

    }
}

// 获取机器人的下一步动作
std::vector<double> Sceduler::getMoveAction(Robot::ptr robot){
    // 保存动作控制
    std::vector<double> control;
    // 根据机器人状态和目的地初始化机器人的控制器
    MoveController m = MoveController(robot->getState(),robot->getTarget(), robot->getThingtype());
    // 为机器人设置控制器
    robot->_controller = std::make_shared<MoveController>(m);
    // 获取控制器
    auto c = robot->_controller;
    // 防撞控制
    for(int k = 0;k<_robot_list.size(); k++){
            // 跳过自己
            if(k == robot->getId()) continue;
            auto other = _robot_list[k];
            c->addObstacle(other->getState());
    }
    // 碰撞分析
    if(c->judgeCollide(robot->getState()))
    {  
        control = c->getCollideControl(robot->getState());
    }
    else{
        control = c->getControl(robot->getState());
    }
    return control;
}
  
