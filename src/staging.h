
#ifndef HW_STAGING_HPP
#define HW_STAGING_HPP

#include <memory>
#include <vector>
#include <queue>
#include <typeinfo>
#include <algorithm>
#include "utils.h"
#include "msg.h"


class Sceduler;
struct Staging_Msg; 

/**
 * @brief 工作台类
 * 
 */
class Staging{
public:
    typedef std::shared_ptr<Staging> ptr;

    Staging(){
        _location.resize(2,-1);
        _id = -1;
        _booked = false;
    }
    ~Staging(){
 
    }

    /**
     * @brief                   常使用的构造方法
     * 
     * @param id                工作台id
     * @param location          坐标
     * @param buy_list          需求的材料列表
     * @param sale              出售的材料
     * @param keep_list         当前持有的材料
     * @param type              工作台类型
     */
    Staging(int id , double location[2], int type, std::shared_ptr<Sceduler> sc)
    : _id(id), _type(type)
    {
        _sc = sc;
        _location.resize(2);
        _location[0] = location[0]; 
        _location[1] = location[1]; 
        switch (type)
        {
            case 1:
                _buy_list = 0;
                _sale = 1;
                break;
            case 2:
                _buy_list = 0;
                _sale = 2;
                break;    
            case 3:
                _buy_list = 0;
                _sale = 4;
                break;
            case 4:
                _buy_list = 1 & 2;
                _sale = 8;
                break;
            case 5:
                _buy_list = 1 & 4;
                _sale = 16;
                break;
            case 6:
                _buy_list = 2 & 4;
                _sale = 32;
                break;
            case 7:
                _buy_list = 8&16&32;
                _sale = 64;
                break;
            case 8:
                _buy_list = 64;
                _sale = 0;
            case 9:
                _buy_list = 1&2&4&8&16&32&64;
                _sale = 0;
            default:
                _buy_list = 0;
                _sale = 0;
                break;
        }
        
    }

    /**
     * @brief                   获取原材料
     * 
     * @param type              获取的原材料
     * @return int :            -1 :原材料栏以满
     *                          -2:原材料类型错误
     *                          50:买成功,还能继续卖
     *                          0-49 :下一个去哪
     *                          100:买成功,但没有下一个地方推荐
     */
    int buy(int type);

    /**
     * @brief                   售出成品
     * 
     * @return int               1-7:成品类型,
     *                          -1  :没生产好       
     */
    int sale();

    /**
     * @brief 根据传入的消息更新staging
     * 
     */
    bool refresh(Staging_Msg& msg);

    /// 工作台状态(待定,状态可能重复,可以改成_buy_list模式)
    enum STATUES{
        IDLE = 0,/// 空闲状态,生产栏为空,原材料不满,且没有机器人正在前往
        GOODS = 1,/// 等待机器人前往送原料
        PRODUCT = 2/// 生产完成,等待机器人收取
    };

    int getId(){return _id;}
    std::vector<double>& getLocation(){return _location;}
    int getKeepList(){return _keep_list;}
    int getBuyList(){return _buy_list;}
    int getSale(){return _sale;}
    STATUES getStates(){return _states;}
    int getType(){return _type;}
    int getNeedList(){return _need_list;}
    int getRestTime(){return _rest_time;}
    bool hasProduce(){
        // 如果没机器人预定
        if(!_booked)
            return _has_product;
        // 如果有机器人预定
        else 
            return false;
    }
    bool  isBooked(){return _booked;}

    void setId(int id){_id = id;}
    void setLocation(double location[2]);
    void setBuyList(int buy_list){_buy_list = buy_list;}
    void setSale(int sale){_sale = sale;}
    void book() {
        _booked = true;
    }


    /**
     * @brief               获取为机器人推荐的下一个目的地
     * 
     * @return int          目的地的id
     */
    int getSuggest(bool e);

    /**
     * @brief               修改需求列表,减去需求
     * 
     * @param need          减去的需求材料(该原材料已经有机器人运送)
     * @return int          返回时候成功.
     */
    int delNeed(int need);

    /**
     * @brief                   判断工作台是否需要该类原料
     * 
     * @param need              原料
     * @return int              0:  该类原料已拥有
     *                          -1: 该类原料不需要
     *                          1:  该类需要且未拥有
     */         
    int ifNeed(int need);
 
    /**
     * @brief                   为推荐列表添加工作台
     * 
     * @param stag              pair类型,first为到目标工作台的最短帧数,second为目标工作台_id; 
     */
    void addNext(std::pair<int,int> stag){
        _nextlist.push_back(stag);
    }
    std::vector<std::pair<int,int>>& getNext(){return _nextlist;}

    void addNear(std::pair<int,int> stag){
        _nearlist.push_back(stag);
        
    }
    std::vector<std::pair<int,int>>& getNear(){return _nearlist;}
    void reverseNear(){reverse(_nearlist.begin(),_nearlist.end());}


private:
    /// 工作台id
    int _id;

    /// 工作台坐标
    std::vector<double> _location;

    /// 原材料列表:从无材料至材料7分别为:0,2,4,8,16,32,64,128
    int _buy_list;

    /// 出售商品同_buy_list;
    int _sale;

    /// 当前持有的原材料(真正持有的,不包括正在运送的)
    int _keep_list;

    /// 当前缺少的材料(有机器人正在送某个材料,就将该材料减去)
    int _need_list;

    /// 剩余生产时间(按桢率,-1为没有生产,0为生产格满了,1-n表示剩余时间)
    int _rest_time;

    /// 当前是否有产品
    bool _has_product;

    /// 工作台类型1-9 : 1,2,4,8,16,32,64,128,256
    int _type;

    // 状态,等待机器人来取产品
    bool _booked;

    STATUES _states;
    
    /// 按距离从大到小顺序保存最近的25个节点,插入时判断,是否够25个,够25个判断新数值是否比原来的小,小就插入
    std::vector< std::pair<int,int> > _nearlist;

    /// 按距离从小到大保存取到当前工作台货物后,下一个可以交货的节点
    std::vector< std::pair<int,int> > _nextlist;

    /**
     * @brief                   开始生产
     * @details                 清空持有列表,更改状态
     */
    void produce();

    std::shared_ptr<Sceduler> _sc;
    
};

#endif //HW_STAGING_HPP