#ifndef HW_ROBOT_HPP
#define HW_ROBOT_HPP

#include <vector>
#include<vector>
#include<cmath>
#include<algorithm>
#include<memory>

#include "msg.h"
#include "movecontrol.h"

class MoveController;
class Sceduler;
class Robot
{
public:
    typedef std::shared_ptr<Robot> ptr;

    Robot(int i,int spotid,int type, double timef, double collisonf,double w, double vx, double vy,
     double direction, double x, double y):_id(i),_spotid(spotid),_thingtype(type),_timef(timef),_collisionf(collisonf),
     _w(w),_direction(direction)  //完成初始位置
    {
        _location.resize(2,-1);
        _location[0] = x;
        _location[1] = y;
        _v[0] = vx;
        _v[1] = vy;
        _controller = nullptr;
        
    }
    Robot(int i, double x, double y)
    {
        _location.resize(2,-1);
        _location[0] = x;
        _location[1] = y;
        _id = i;
    }

    void setSpotid(int id){
        _spotid = id;
    }


    //设置参数
    void setTimef(double timef);
    void setCollisionf(double collidf);
    void setW(double w);
    void setV(double vx, double vy);
    void setDireciton(double direction);
    void setLocation(double locationx, double locationy);
    void setA(double a);
    void setB(double b);
    void setJ(double j);
    void setTarget(int index);
    void setThingtype(int thingtype);
    void setSc(std::shared_ptr<Sceduler> sc);

    int getId();
    int getSpotid();
    int getTargetId();
    std::vector<double>& getTarget();
    double getTimef();
    double getCollisionf();
    double getW();
    std::vector<double> getV();
    double getDireciton();
    std::vector<double> getLocation();
    double getA();
    double getB();
    double getJ();
    int getThingtype(){
        return _thingtype;
    }
    
    
    
   //运动控制
    std::vector<double> actionModel(std::vector<double>& state,std::vector<double> control, double dt);
    /**
     * @brief Get the Control object 到目标需要的操作
     * @param target 目的坐标
     * @param  obstacle 障碍物坐标
     * @param  control_mode 控制模式，0，默认到达  1，dwa算法
     * @return vector<double> 
     */
    std::vector<double> moveControl(std::vector<double>& target, std::vector<std::vector<double>> obstacle,int control_mode);

    /**
     * @brief Get the State object
     * 
     * @return vector<double> 
     */
    std::vector<double> getState();
    void refresh(Robot_Msg& mesg);
    double getYaw();



    //运动
    std::vector<double> forward(double newv,  double dt);
    std::vector<double> rotate(double neww, double dt);

    void buy(int thingType);
    void sell();
    void destroy();



    //到达边界判断
    bool getEdge();

    //到达工作台判断
    bool arriveTarget();

    //

    ~Robot(){}

    std::shared_ptr<MoveController> _controller;

private:
    /* data */
    int _id;
    int _spotid;//当前工作台id,0表示未进入工作台范围
    int _thingtype;//物品种类 0表示没有
    double _timef; //时间损失系数
    double _collisionf; //碰撞损失系数
    double _w; //角速度
    std::vector<double> _v;  //线速度
    double _direction; //朝向[-pi, pi]
    std::vector<double> _location; //位置x,y
    double _a;//加速度 --
    double _b;//角加速度 --
    double _r;//半径 --
    double _m;//质量 -- 
    double _j;//转动惯量--
    int _target_id;// 目标的id
    
    std::shared_ptr<Sceduler> _sc;

    std::vector<double> _target;//目标地点x,y
    

};

#endif //HW_SCEDULER_HPP






