#include "robot.h"
#include "utils.h"
#include "sceduler.h"
#include "movecontrol.h"

//设置参数
void Robot::setTimef(double timef)
{
    _timef = timef;
}    
void Robot::setCollisionf(double collidf){
    _collisionf = collidf;
}
void Robot::setW(double w){
    _w = w;
}
void Robot::setV(double vx, double vy){
    _v[0] = vx;
    _v[1] = vy;
}
void Robot::setDireciton(double direction){
    _direction = direction;
}
void Robot::setLocation(double locationx, double locationy){
    _location[0] = locationx;
    _location[1] = locationy;
}
void Robot:: setA(double a)
{
    _a = a;
}
void Robot::setB(double b){
    _b = b;
}
void Robot::setJ(double j){
    _j = j;
}
void Robot::setTarget(int index)
{
    auto t =_sc->getStaging(index);
    _target = t->getLocation();
    _target_id = index;

}

void Robot::setThingtype(int thingtype)
{
    _thingtype = thingtype;
}
void Robot::setSc(Sceduler::ptr sc){
    _sc = sc;
}



//获取参数
int Robot::getId(){
    return _id;
}
int Robot::getSpotid()
{
    return _spotid;
}
int Robot::getTargetId()
{
    return _target_id;
}
std::vector<double>& Robot::getTarget(){
    return _target;
}
double Robot::getTimef(){
    return _timef;
}
double Robot::getCollisionf(){
    return _collisionf;
}
double Robot::getW(){
    return _w;
}
std::vector<double> Robot::getV(){
    return _v;
}
double Robot::getDireciton(){
    return _direction;
}
std::vector<double> Robot::getLocation(){
    return _location;
}
double Robot::getA(){
    return _a;
}
double Robot::getB(){
    return _b;
}
double Robot::getJ(){
    return _j;
}



//动作




/**
 * 机器人运动学模型
 * @param state 运动状态量---x,y,yaw,v,w:坐标x,y，与目标夹角，线速度，角速度
 * @param control 控制量---v,w,线速度和角速度
 * @param dt 采样时间
 * @return 下一步的状态
 */

std::vector<double> Robot::actionModel(std::vector<double>& state, std::vector<double> control, double dt)
{
    state[0] += control[0] * cos(state[2]) * dt;
    state[1] += control[0] * sin(state[2]) * dt;
    state[2] += control[1] * dt;
    state[3] = control[0];
    state[4] = control[1];

    return state;
}


//获得当前状态列表
std::vector<double> Robot::getState()
{
    std::vector<double> state;
    std::vector<double> loc= getLocation();
    double yaw  = getYaw();
    state.push_back(loc[0]);
    state.push_back(loc[1]);
    state.push_back(yaw);
    std::vector<double> v = getV();
    double vall = sqrt(pow(v[0],2)+pow(v[1], 2));  //合速度
    double w= getW();
    state.push_back(vall);
    state.push_back(w);
    return state;
}

void Robot::refresh(Robot_Msg& mesg)
{
    _spotid = mesg._staging_id;//当前工作台id,0表示未进入工作台范围
    _thingtype = mesg._keep ;//物品种类 0表示没有
    if(_thingtype == 0) 
    {
        _r = RR;
        _m = MR;
        _j = JR;
        _a = FMAX/_m;
        _b = MMAX/_j;

    }
    else{
        _r = RC;
        _m = MC;
        _j = JC;
        _a = FMAX/_m;
        _b = MMAX/_j;
    }
    _timef = mesg._time_value; //时间损失系数
    _collisionf= mesg._collide_value; //碰撞损失系数
    _w=mesg._w; //角速度
    _v= {mesg._v_x, mesg._v_y};  //线速度
    _direction=mesg._angle; //朝向[-pi, pi]
    std::vector<double> _location={mesg._x, mesg._y}; //位置x,y
    
}







//计算与目标夹角
double Robot::getYaw()
{
    double targetx = _target[0];
    double targety = _target[1];
    std::vector<double> loc= getLocation();
    double dx = _target[0]-loc[0];
    double dy = _target[1] -loc[1];

    double dire = getDireciton();
    double alpha = atan2(dy, dx);
    return dire - alpha; 
}


//传入命令，利用机器人运动模型获得新状态
std::vector<double> Robot::forward(double newv,  double dt)
{
    std::vector<double> control;
    control.push_back(newv);
    control.push_back(0);
    std::vector<double> state  = getState();
    std::vector<double> newstate = actionModel(state, control, dt);
    return newstate;
}

std::vector<double> Robot::rotate(double neww, double dt)
{
    std::vector<double> control;
    control.push_back(neww);
    control.push_back(0);
    std::vector<double> state  = getState();
    std::vector<double> newstate = actionModel(state, control, dt);
    return newstate;
}




//改变机器人半径，转动惯量，持有物品状态
void Robot::buy(int thingType)
{
    _r = RC;
    _m = PI*pow(_r,2);
    _j = DENS*PI*pow(_r, 4);
    _thingtype = thingType; 
}
void Robot::sell(){
    _r = RR;
    _m = PI*pow(_r,2);
    _j = DENS*PI*pow(_r, 4);
    _thingtype = 0; 
}
void Robot::destroy(){
    _r = RR;
    _m = PI*pow(_r,2);
    _j = DENS*PI*pow(_r, 4);
    _thingtype = 0; 
}


bool Robot::getEdge(){
    
}

bool Robot::arriveTarget()
{
    return sqrt(pow(_location[0]-_target[0],2)+ pow(_location[1]-_target[1],2)) < RS;
}


