#include "staging.h"
#include "sceduler.h"

int Staging::buy(int type){
    // 是不是在需求列表里
    if( !(type & _buy_list) ){
        return -2;
    }
    // 是不是已经满了
    if( type & _keep_list){
        return -1;
    }
    // 增加持有列表
    _keep_list |= _keep_list;
    // 全满了就生产
    if(!(_keep_list ^ _buy_list)){
        produce();
    }
    return getSuggest(false);
}

int Staging::sale(){
    if(!_has_product){
        return -1;
    }
    _has_product = 0;
    _booked = false;
    return getSuggest(true);
}

void Staging::produce(){
    _keep_list = 0;
    _need_list = _buy_list;
    
    // _states = PRODUCING;
}

bool Staging::refresh(Staging_Msg& msg){
    if(msg._id != _id ){
        return false;
    }
    _rest_time = msg._remaining_time;
    _keep_list = msg._keep_list;
    _has_product = msg._saled;
}


int Staging::ifNeed(int need){
    if(!(need & _buy_list)){
        return -1;
    }
    if(need & _need_list){
        return 0;
    }
    return 1;
}

int Staging::delNeed(int need){
    int rt = ifNeed(need);
    if(rt != 1){
        return rt;
    }
    _need_list |= need;
}

int Staging::getSuggest(bool e){
    // 如果是卖出产品,机器人带着材料走
    if(e){
        for(auto it : _nextlist){
            // 依次获取最近的下级staging
            auto st = _sc->getStaging(it.second);
            if(st->getNeedList() & _sale){
                return it.second;
            }
        }
    // 买原材料,机器人空手走
    } else {
        for(auto it : _nearlist){
            // 依次获取最近的下级staging
            auto st = _sc->getStaging(it.second);
            // 获取产品状态
            int hp = st->hasProduce();
            // 如果有产品且没有被预定
            if(hp){
                return it.second;
            // 如果产品没生产好且没有被预定
            } else if(!st->isBooked()){
                int rt = st->getRestTime();
                // 当下个工作台的距离帧小于机器人到那里的时间
                if(rt > 0 && rt <= it.first ){
                    return it.second;
                }
            } 
        }
        // 如果没有合适的目的地,就返回自己
        return _id;
    }
}