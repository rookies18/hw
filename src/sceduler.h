
#ifndef HW_SCEDULER_HPP
#define HW_SCEDULER_HPP


#include "staging.h"
#include <vector>
#include <string>
#include <memory>
#include <sstream>
#include "robot.h"
#include "msg.h"
#include "utils.h"

class Sceduler :public std::enable_shared_from_this<Sceduler>{
    
public:
    typedef std::shared_ptr<Sceduler> ptr;

    // 映射表,二维数组形式,保存两两工作台之间的距离和最短到达时间
    static std::vector<std::vector<int>> _dis_map;    

    // 帧序号
    static int _times;
    // 当前钱数
    static int _money;
    // 控制台总数
    static int _k;

public:

    // 构造函数,完成六个静态成员变量的初始化
    Sceduler(){
        // 初始化两个消息指针
        _msgr.reset(new MsgR());
        _msgs.reset(new MsgS());
         _staging_list.reserve(50);
        _robot_list.reserve(4);
    }

    ~Sceduler(){}

    /**
     * @brief                           初始化函数,接收首个msg并进行初始化
     */
    void Init();

    /**
     * @brief                           调度函数
     * 
     */
    void run();

    /**
     * @brief                           接收判题器返回的帧,并进行解析和状态更新
     * 
     */
    int getMsg();

    /**         
     * @brief                           将_msgS中的msg发送出去
     */
    void sendMsg();

    /**
     * @brief                           通过读取文件内容,对getMsg进行测试
     * 
     * @return                          
     */
    int getMsg_test();
    int sendMsg_test();

    /**
     * @brief                           获取机器人下一帧的控制指令
     * 
     * @param robot                     机器人id 
     * @return vector<double>           指令
     */
    std::vector<double> getMoveAction(Robot::ptr robot);

    int getMinStag(int index);

    void RobotWork();

    Staging::ptr getStaging(int i){return _staging_list[i];}
    Robot::ptr getRobot(int i){return _robot_list[i];}


private:

        // 控制台列表
    std::vector<Staging::ptr> _staging_list;
    // 机器人列表
    std::vector<Robot::ptr> _robot_list;

    // 每一桢接收到的消息
    MsgR::ptr _msgr;

    // 每一桢需要发送的消息
    MsgS::ptr _msgs;

    // 每一桢未解码的原始消息,按行存储.
    std::vector<std::string> _msg;

    // 发送缓冲区
    std::stringstream _send;


};
#endif //HW_SCEDULER_HPP