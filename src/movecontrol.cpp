#include "movecontrol.h"
/***
 * state:   {x, y, direction, v, w , thingtype}
*/


/***
 * 调度器robot.setTarget后，
 * 创建movecontrol对象初始化,并返回control{v, w},
 * 随后获得机器人新消息
 * 
 * 执行judgeColiide判断是否会碰撞风险，如果有会更新obstacle
 * 如果没有碰撞风险，使用getControl，获得新命令
 * 否则初始化collideController，使用getCollideControl获得命令
 *
*/




/**
 * @brief Construct a new Move Controller:: Move Controller object
 * 
 * @param  state 运动状态量---x,y,yaw,v,w:坐标x,y，与目标夹角，线速度，角速度

    @param target  x,y
 */
MoveController::MoveController(std::vector<double> state, std::vector<double>target, int thingtype)
{

    _target = target;
    //最大加速度，最大角加速度
    std::vector<double> control(2, 0);
    //计算路程，判断临界值
    double v = state[3];
    double yaw = state[2];
    _keep = thingtype;
    bool carry = _keep == 0? 0:1; 
    //到目标距离，根据距离和目前速度来判断该加速还是减速。（直线）
   
    double distance = sqrt(pow(state[0]-_target[0],2)+pow(state[1]-_target[1],2));//到达工作台判定范围
    double radius = carry == 0 ? RR: RC;
    double m  = DENS*PI* pow(radius,2);
    double acc = FMAX/m;
    double be = MMAX/(m*pow(radius,2));// angle acceloration
    double critc_d = VMAX*VMAX/acc; //先加速到最大，再减速到0的临界距离。
    double Brake_d = v*v/(2*acc);  //到了刹车距离

    //初始化
    PID_controller pid(2, 0.01, 30, 0., PI, -PI);
    pidpointer = std::make_shared<PID_controller>(pid);
    DWA dwa = DWA(0.02 , 0, VMAX, 0,  WMAX, 5, acc, be, 0.1, 0.1, 0.1, 0.1, 0.1, RS, 1.5);
    dwapointer = std::make_shared<DWA>(dwa);
    _obstacle.resize(9);
    std::vector<double>  _target = target;


    control[1] =  pid.calOutput(yaw);
    if(distance< critc_d ) //先加速后减速
    {
        if(distance-Brake_d<EPSION ) //小于刹车距离减速
            {
                control[0] = VMMax;  //减速
            }
            else control[0] = VMAX; //加速
    }
    else //先加速，再匀速，再减速
    {
            if(distance-Brake_d<EPSION ) //小于刹车距离减速
            {
                control[0] = VMMax;  //减速
            }
            else control[0] = VMAX; //加速
    }
    
}

/**
 * @brief 根据当前状态输出控制信息
 * 
 * @param msg 
 */
std::vector<double> MoveController::getControl(std::vector<double> state)
{
     //最大加速度，最大角加速度

    std::vector<double> control(2, 0);
    //计算路程，判断临界值
    double v = state[3];
    double yaw = state[2];
    bool carry = _keep == 0? 0:1; 
    //到目标距离，根据距离和目前速度来判断该加速还是减速。（直线）
    double distance = sqrt(pow(state[0]-_target[0],2)+pow(state[1]-_target[1],2));//到达工作台判定范围
    double radius = carry == 0 ? RR: RC;
    double m  = DENS*PI* pow(radius,2);
    double acc = FMAX/m;
    double be = MMAX/(m*pow(radius,2));// angle acceloration
    double critc_d = VMAX*VMAX/acc; //先加速到最大，再减速到0的临界距离。
    double Brake_d = v*v/(2*acc);  //到了刹车距离


    //平动逻辑
    if(distance< critc_d ) //先加速后减速
    {
        if(distance-Brake_d<EPSION ) //小于刹车距离减速
            {
                control[0] = VMMax;  //减速
            }
            else control[0] = VMAX; //加速
    }
    else //先加速，再匀速，再减速
    {
            if(distance-Brake_d<EPSION ) //小于刹车距离减速
            {
                control[0] = VMMax;  //减速
            }
            else control[0] = VMAX; //加速
    }

    //转动逻辑
    //旋转  需要控制yaw始终为0，采用pid控制
    control[1] =  pidpointer->calOutput(yaw);

    return control;
    /*
    if(abs(yaw) <EPSION)control[1] = 0; //无夹角，不旋转
    else{ 
        double t_rtomax = WMAX/be; //time:from 0 to the maixum  W
        double brake_yaw = msg._w*msg._w/(2*be);
        if(yaw- brake_yaw< EPSION) 
        control[1] = -WMAX;
        else control[1] = WMAX;
    }
    return control;
    */


}


std::vector<double> MoveController::getCollideControl(std::vector<double> state)
{
    
    std::vector<double> control(2, 0);
    //计算路程，判断临界值
    double v = state[3];
    double yaw = state[2];
    bool carry = _keep == 0? 0:1; 
    //到目标距离，根据距离和目前速度来判断该加速还是减速。（直线）
    
    double distance = sqrt(pow(state[0]-_target[0],2)+pow(state[1]-_target[1],2));//到达工作台判定范围
    double radius = carry == 0 ? RR: RC;
    double m  = DENS*PI* pow(radius,2);
    double acc = FMAX/m;
    double be = MMAX/(m*pow(radius,2));// angle acceloration
    double critc_d = VMAX*VMAX/acc; //先加速到最大，再减速到0的临界距离。
    double Brake_d = v*v/(2*acc);  //到了刹车距离

    auto res = dwapointer->dwaControl(state, _target,_obstacle);
    control[0] = res.first[0];
    control[1] = res.first[1];
    return control;
}

/***
 * @brief 传入其他机器人消息，判断是否即将发生碰撞，并设置最近的为障碍
 *  
*/
bool MoveController::judgeCollide(std::vector<double>state)
{

    bool carry = _keep == 0? 0:1; 
    double radius = carry == 0 ? RR: RC;
    double min_dis = 10000;
    int id = 0;
    bool flag = false;
    std::vector<double>tmp(2, 0);
    

    //判断是否即将和墙装上
    double t = 0.05;
    double nx= state[0]+ state[3]*cos(state[2])*t; 
    double ny = state[1] + state[3]*sin(state[2])*t;

    //右边墙
    if(nx >50) _obstacle.emplace_back(std::vector<double>{50, state[1]});
    //左边墙
    if(nx < 0) _obstacle.emplace_back(std::vector<double>{0, state[1]});
    //上
    if(ny >50) _obstacle.emplace_back(std::vector<double>{state[0], 50});
    //下
    if(ny <0) _obstacle.emplace_back(std::vector<double>{state[0], 0});

    

    for(auto object : _obstacle)
    {
        double dis = sqrt(pow(state[0]-object[0],2)+pow(state[1]-object[1],2));
        if(  dis < 3*radius);//到达其他机器人< 4*radius) 
        {
            flag = true;
        }
    }
    return flag;
}


void MoveController::addObstacle(std::vector<double> obstate)
{
    _obstacle.emplace_back(obstate[0], obstate[1]);
}