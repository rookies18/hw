#ifndef HW_MVC_HPP
#define HW_MVC_HPP

#include <vector>
#include "robot.h"
#include "utils.h"
#include "movecontrol.h"
#include "method1_dwa.h"
#include "PID_controller.h"
#include "msg.h"


/***
 * 调度器robot.setTarget后，
 * 创建movecontrol对象初始化,并返回control{v, w},
 * 随后获得机器人新消息
 * 执行judgeColiide判断是否会碰撞风险，如果有会更新obstacle
 * 如果没有碰撞风险，使用getControl，获得新命令
 * 否则初始化collideController，使用getCollideControl获得命令
 *
*/
class MoveController
{
public:
    //重新设置目的地后
    MoveController(std::vector<double> state, std::vector<double> target, int thingtype);
    //地图初始化后
    //MoveController(vector<double> state, vector<double> target);
    //接收当前状态
    void updateState(Robot_Msg msg);
    //获取下一步指令
    std::vector<double> getControl(std::vector<double> state);
    std::vector<double> getCollideControl(std::vector<double> state);
    bool judgeCollide(std::vector<double>state);
    void addObstacle(std::vector<double> obstate);
    ~MoveController(){}
private:
    double previous_error = 0;
    double integral = 0;
    std::vector<double> _target;
    int _keep;
    std::vector<std::vector<double>>_obstacle; //最可能障碍物坐标
    std::shared_ptr<PID_controller> pidpointer;
    std::shared_ptr<DWA> dwapointer;
};

#endif//HW_MVC_HPP