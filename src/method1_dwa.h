#ifndef HW_DWA_HPP
#define HW_DWA_HPP
#include<iostream>
#include<vector>
#include<cmath>
#include<algorithm>


 


class DWA {


public:
    DWA(double dt, double vMin, double vMax, double wMin, double wMax, double predictTime, double aVmax, double aWmax,
        double vSample, double wSample, double alpha, double beta, double gamma, double radius, double judgeDistance);

    std::vector<double> kinematicModel(std::vector<double> state, std::vector<double>control,double dt);

    std::pair<std::vector<double>,std::vector<std::vector<double>>> dwaControl(std::vector<double> state, 
                std::vector<double> goal, std::vector<std::vector<double>> obstacle);

private:
    std::vector<double> calVelLimit();
    std::vector<double> calAccelLimit(double v, double w);
    std::vector<double> calObstacleLimit(std::vector<double> state, std::vector<std::vector<double>> obstacle);
    std::vector<double> calDynamicWindowVel(double v, double w,std::vector<double> state, std::vector<std::vector<double>> obstacle);
    double _dist(std::vector<double> state, std::vector<std::vector<double>> obstacle);
    std::vector<std::vector<double>> trajectoryPredict(std::vector<double> state, double v, double w);
    std::pair<std::vector<double>,std::vector<std::vector<double>>> trajectoryEvaluation(std::vector<double> state,
                             std::vector<double> goal, std::vector<std::vector<double>> obstacle);

    double _heading(std::vector<std::vector<double>> trajectory,std::vector<double> goal);
    double _velocity(std::vector<std::vector<double>> trajectory);
    double _distance(std::vector<std::vector<double>> trajectory,std::vector<std::vector<double>> obstacle);


private:
    double dt; //采样时间
    double v_min,v_max,w_min,w_max; //线速度角速度边界
    double predict_time;//轨迹推算时间长度
    double a_vmax,a_wmax; //线加速度和角加速度最大值
    double v_sample,w_sample; //采样分辨率
    double alpha,beta,gamma; //轨迹评价函数系数
    double radius; // 用于判断是否到达目标点
    double judge_distance; //若与障碍物的最小距离大于阈值（例如这里设置的阈值为robot_radius+0.2）,则设为一个较大的常值
};
#endif//HW_DWA_HPP