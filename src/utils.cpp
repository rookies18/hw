#include "utils.h"
#include<algorithm>


/***
 * @param now     目前x,y
 * @param target  目标点，x,y 
*/
double _dist(std::vector<double>now ,std::vector<double> target)
{
    double dx  =target[0] - now[0];
    double dy = target[1] - now[1];
    return sqrt(pow(dx,2)+ pow(dy, 2)); 
    
}


int timeMin(std::vector<double>now, std::vector<double> target){
    double dis  = _dist(now, target)- 2*RS;//到达工作台判定范围
    double radius = RC;
    double m  = DENS*PI* pow(radius,2);
    double acc = FMAX/m;
    double critc_d = VMAX*VMAX/acc;
    double t; 
    if(dis-critc_d<= EPSION)
    t = sqrt(dis/acc);//时间
    else 
    {
    t = (dis-critc_d)/VMAX + 2*VMAX/acc;
    }

    //秒数*每秒50帧
    int framenum = t*50;
    return framenum;   //按照先加速后减速
}

/***
 * 计算与目标夹角
 * state:{{x, y, direction, v, w }}
*/
double _getYaw(std::vector<double>state, std::vector<double> target)
{

    double dx = target[0]-state[0];
    double dy = target[1] -state[1];

    double dire = state[2];
    double alpha = atan2(dy, dx);
    return dire - alpha; 
}
