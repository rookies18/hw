#ifndef HW_MSG_HPP
#define HW_MSG_HPP


#include <memory>
#include <vector>
#include <sstream>
#include "staging.h"



/**
 * @brief                       staging相关的消息
 *  
 * @details                     保存所有的Staging返回信息
 */
struct Staging_Msg
{   
    int _id;                    // id 
    double _x;                  // x坐标
    double _y;                  // y坐标
    int _remaining_time;        // 剩余生产时间
    int _keep_list;             // 当前持有的原料
    int _saled;                 // 是否生产完成

    std::string toString(){
        char msg[1024];
        sprintf(msg,"%d %.8lf %.8lf %d %d %d",_id,_x,_y,_remaining_time,_keep_list,_saled);
        return msg;
    }
};

/**
 * @brief Reboot相关的消息
 * 
 */
struct Robot_Msg
{
    int _id;                    // 当前机器人id
    int _staging_id;            // 附近工作台
    int _keep;                  // 携带的材料
    double _time_value;         // 时间系数
    double _collide_value;      // 碰撞系数
    double _w;                  // 角速度
    double _v_x;                // x线速度
    double _v_y;                // y线速度
    double _angle;              // 角度(朝向)
    double _x;                  // x坐标
    double _y;                  // y坐标
    std::string toString(){
        char msg[1024];
        sprintf(msg,"%d %d %.8lf %.8lf %.8lf %.8lf %.8lf %.8lf %f %.8lf",
            _staging_id,_keep,_time_value,_collide_value,_w,_v_x,_v_y,_angle,_x,_y);
        return msg;
    }

};


/**
 * @brief                               服务器发送的消息(SDK接受的消息)
 * 
 * @details                             核心方法为初始化和解析,解析结果保存在成员变量里
 *                                      应当只有一个实例(每次只会处理一个消息)
 */
class MsgR{
public:

    typedef std::shared_ptr<MsgR> ptr;

    MsgR(){}
    ~MsgR(){}

    /**
     * @brief                           初始化(重置)Msg
     * 
     * @details                         每次接收新的判题器消息前都要进行初始化,将各个成员重置.
     */
    void Init();


    /**
     * @brief                           判题器返回信息的解析
     * 
     * 
     * @param recv                      接收到的消息
     * @return int                      -1,解析错误; 0 成功；1,待定
     */
    int Parse(std::string recv);

private:
    int _frame_number;                  // 桢序号
    int _money;                         // 钱数
    int _staging_number;                // 控制台数
    std::vector<Staging_Msg> _st_m;     // 全部的控制台信息
    std::vector<Robot_Msg> _rb_m;      // 全部的机器人信息
    bool _init;                         // 是否初始化
};



struct Command {
    std::string _instruct;
    int _id;
    double _arg;
    bool _hasThird;
    

    // 将Command结构体输出为字符串形式,可以直接写到用户输出里
    std::string toString(){
        char msg[512];
        if(_hasThird){
            sprintf(msg,"%s %d %.8lf",_instruct.c_str(),_id,_arg);
            return msg;
        } else {
            sprintf(msg,"%s %d",_instruct.c_str(),_id);
        }
    }
};


/**
 * @brief                               需要发送的消息
 * 
 */
class MsgS{
public:
    typedef std::shared_ptr<MsgS> ptr;

    // 常量,用来直接字符串加减


    MsgS(){}
    ~MsgS(){}

    int set_FN(int fn){_frame_number = fn;}
    int get_FN(){return _frame_number;}

    /**
     * @brief                   加入一行新的命令
     * 
     * @param cmd               命令结构体
     * @return true             成功
     * @return false            失败
     */
    bool addCMD(Command& cmd){
        _command.push_back(cmd);
    } 

    /**
     * @brief                   获取命令列表
     * 
     * @return                  返回列表的引用
     */
    std::vector<Command>& getCmd(){return _command;}

    /**
     * @brief                   清空命令列表
     * 
     */
    void clear(){_command.clear();}
private:
    int _frame_number;                  // 桢序号
    std::vector<Command> _command;      // 全部命令
};

#endif //HW_MSG_HPP