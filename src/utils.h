#ifndef HW_UTILS_HPP
#define HW_UTILS_HPP

#include <vector>

#define PI 3.1415926
#define RS 0.4
#define RR 0.45
#define RC 0.53
#define DENS 20
#define MR 12.72345003
#define JR 2.576498631075
#define MC 17.6494672268
#define JC 4.95773534400812
//判定半径，机器人半径，负载半径,密度,原质量，原转动惯量，负载质量，负载转动惯量


//最大速度，最大后退速度，最大旋转，最大牵引力，最大力矩
#define VMAX 6
#define VMMax -2
#define WMAX 3.1415926
#define FMAX 250
#define MMAX 50
#define EPSION 1e-5



double _dist(std::vector<double>now ,std::vector<double> target);
int timeMin(std::vector<double>now, std::vector<double> target);
double _getYaw(std::vector<double>state, std::vector<double> target);

#endif //HW_UTILS_HPP